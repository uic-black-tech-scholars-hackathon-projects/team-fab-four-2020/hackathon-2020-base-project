﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceValue : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public int getValue()
    {
        int iValue = -1;
        Vector3 dieRotation = gameObject.transform.eulerAngles;

        dieRotation = new Vector3(Mathf.RoundToInt(dieRotation.x), Mathf.RoundToInt(dieRotation.y), Mathf.RoundToInt(dieRotation.z));
        if (dieRotation.x == 180 && dieRotation.y == 270 || 
            dieRotation.x == 0 && dieRotation.z == 90) {
            iValue = 1;
        } else if (dieRotation.x == 270) {
            iValue = 2;
        } else if (dieRotation.x == 180 && dieRotation.z == 0 ||
            dieRotation.x == 0 && dieRotation.z == 180) {
            iValue = 3;
        } else if (dieRotation.x == 180 && dieRotation.z == 180 || 
            dieRotation.x == 0 && dieRotation.z == 0) {
            iValue = 4;
        } else if (dieRotation.x == 90) {
            iValue = 5;
        } else if (dieRotation.x == 0 && dieRotation.z == 270 || 
            dieRotation.x == 180 && dieRotation.z == 90) {
            iValue = 6;
        }

        Debug.Log("eulerAngles: " + dieRotation.x + ", " + dieRotation.y + ", " + dieRotation.z + " Value: " + iValue);

        return iValue;
    }
}
