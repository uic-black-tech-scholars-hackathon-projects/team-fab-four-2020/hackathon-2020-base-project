﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DropDown_Welcome : MonoBehaviour{
    public Dropdown CourseDropDown;
    public Dropdown GradeDropDown;
    public string scenename;
    public Text Error; 

    public void Dropdownevent() {
  
        if (CourseDropDown.value == 2 & GradeDropDown.value == 2)
        {
            // This function should only go to the UserNameInput scene for the first page. 
            scenename = "UserNameInput";
            UnityEngine.SceneManagement.SceneManager.LoadScene(scenename);
        }
        if (CourseDropDown.value == 0 & GradeDropDown.value == 0)
        {

            Error.text = "Select A Course and/or Grade!";
        }
        else
        {

            Error.text = "This portion of the game has not been completed :(";
        }
    
    }
}

