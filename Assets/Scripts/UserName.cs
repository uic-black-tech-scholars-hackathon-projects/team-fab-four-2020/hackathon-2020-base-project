﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserName : MonoBehaviour {

    public InputField Uname;
    public int Level;
    public string HelloUser;
    public Text mystat;

    public void Getname()
    {
        HelloUser = Uname.text;

    }

    public void Getlevel()
    {
        Level = 3;

    }
    public void SaveName()
    {
        Save_load_name.SaveName(this);
    }

    public void LoadName()
    {
        AssignName data = Save_load_name.LoadName();
        Level = data.Level; 
        HelloUser = data.HelloUser;
        mystat.text = "Welcome " + Uname.text+ "!  You are on Level: " + Level;

    }
 
    

}
