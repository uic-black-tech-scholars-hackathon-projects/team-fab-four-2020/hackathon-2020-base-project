﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class Save_load_name {
 
    public static void SaveName (UserName user)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/user.yay";
        FileStream stream = new FileStream(path, FileMode.Create);

        AssignName data = new AssignName(user);

        formatter.Serialize(stream, data);
        stream.Close();

    }
    public static AssignName LoadName ()
    {
        string path = Application.persistentDataPath + "/user.yay";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            AssignName data = formatter.Deserialize(stream) as AssignName;
            stream.Close();
            return (data);
        }
        else
        {
            Debug.LogError("Save File Not Found In: " + path);
            return null;
        }
    }

}
